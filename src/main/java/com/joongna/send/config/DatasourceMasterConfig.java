package com.joongna.send.config;

import org.mariadb.jdbc.MariaDbDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Package :: com.joongna.send.config
 * Developer :: Kang Tai-kyu(teckham7)
 * Date :: 2019-06-05
 * Description :: Master DB JPA Configuration
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "masterEntityManagerFactory",
        transactionManagerRef = "masterTransactionManager",
        basePackages = "com.joongna.send.repository.master"
)
public class DatasourceMasterConfig {
    private final Environment environment;

    public DatasourceMasterConfig(Environment environment) {
        this.environment = environment;
    }


    @Primary
    @Bean(name = "masterDatasource")
    public DataSource masterDatasource() {
        return DataSourceBuilder.create()
                .driverClassName(environment.getProperty("database.master.datasource.driver-class-name"))
                .url(environment.getProperty("database.master.datasource.url"))
                .username(environment.getProperty("database.master.datasource.username"))
                .password(new String(Base64.getDecoder().decode(Objects.requireNonNull(environment.getProperty("database.master.datasource.password")))))
                .type(MariaDbDataSource.class)
                .build();
    }

    @Primary
    @Bean(name = "masterEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(
            EntityManagerFactoryBuilder entityManagerFactoryBuilder, @Qualifier("masterDatasource") DataSource masterDatasource)  {
        Map<String, String> propertiesMap = new HashMap<>();
        propertiesMap.put("hibernate.physical_naming_strategy" , "org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy");
        propertiesMap.put("hibernate.implicit_naming_strategy" , "org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy");

        return entityManagerFactoryBuilder
                .dataSource(masterDatasource)
                .packages("com.joongna.send.data.entity")
                .properties(propertiesMap)
                .persistenceUnit("master_db")
                .build();
    }

    @Primary
    @Bean("masterTransactionManager")
    public PlatformTransactionManager masterTransactionManager(
            @Qualifier("masterEntityManagerFactory") EntityManagerFactory entityManagerFactory)    {
        return new JpaTransactionManager(entityManagerFactory);
    }
}
