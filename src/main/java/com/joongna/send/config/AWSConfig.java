package com.joongna.send.config;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.joongna.send.properties.AwsS3Properties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class AWSConfig {

    private final AwsS3Properties awss3Properties;

    @Autowired
    public AWSConfig(AwsS3Properties awss3Properties) {
        this.awss3Properties = awss3Properties;
    }

    @Bean(name = "s3AwsCredentials")
    public AWSCredentials s3AwsCredentials() {
        return new BasicAWSCredentials(awss3Properties.getKey().getAccess(), awss3Properties.getKey().getSecret());
    }

    @Bean
    public AmazonS3 amazonS3(@Qualifier("s3AwsCredentials") AWSCredentials s3AwsCredentials) {
        return AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(s3AwsCredentials))
                .withRegion(Regions.AP_NORTHEAST_2)
                .build();
    }
}
