package com.joongna.send.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties(prefix = "aws")
public class AwsS3Properties {
    private Bucket bucket = new Bucket();
    private Folder folder = new Folder();
    private Key key = new Key();

    @Getter
    @Setter
    public static class Bucket {
        private String folder;
        private String name;
    }

    @Getter
    @Setter
    public static class Folder {
        private String ori;
        private String thum;
        private String waterMark;
    }

    @Getter
    @Setter
    public static class Key {
        private String access;
        private String secret;
    }
}

