package com.joongna.send.controller;

import com.joongna.send.data.entity.SampleEntity;
import com.joongna.send.service.sample.SampleService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Package :: com.joongna.send.controller
 * Developer :: Kang Tai-kyu(teckham7)
 * Date :: 2019-06-07
 * Description ::
 */
@RestController
@RequestMapping("/sample")
public class SampleController {

    private final SampleService sampleService;

    public SampleController(SampleService sampleService) {
        this.sampleService = sampleService;
    }


//    @GetMapping("/paramTest")
//    public ResponseEntity<List<SampleEntity>> sample(@RequestBody SampleParameter parameter)  {
//        return new ResponseEntity<>(sampleService.getSampleData(parameter), HttpStatus.OK);
//    }

    @GetMapping("/test")
    public ResponseEntity<List<SampleEntity>> sample(Long categorySeq)    {
        return new ResponseEntity<>(sampleService.getSampleData(categorySeq), HttpStatus.OK);
    }

//    @GetMapping("/one")
//    public ResponseEntity sample()    {
//        return new ResponseEntity(sampleService.getSampleData(), HttpStatus.OK);
//    }
}
