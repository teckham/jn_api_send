package com.joongna.send.service.sample;

import com.joongna.send.data.entity.SampleEntity;
import com.joongna.send.repository.slave.SampleSlaveRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Package :: com.joongna.send.service.sample
 * Developer :: Kang Tai-kyu(teckham7)
 * Date :: 2019-06-07
 * Description ::
 */
@Service
    public class SampleService {

        private final SampleSlaveRepository slaveRepository;

    public SampleService(SampleSlaveRepository slaveRepository) {
        this.slaveRepository = slaveRepository;
    }

    public List<SampleEntity> getSampleData(Long categorySeq) {
        return slaveRepository.findByCategorySeq(categorySeq);
    }
}
