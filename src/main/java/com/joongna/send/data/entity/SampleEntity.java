package com.joongna.send.data.entity;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * Package :: com.joongna.send.data.entity
 * Developer :: Kang Tai-kyu(teckham7)
 * Date :: 2019-06-07
 * Description ::
 */
@Getter
@Entity
@Table(name = "category")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SampleEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long categorySeq;
    private String categoryName;
    @Temporal(TemporalType.TIMESTAMP)
    private Date regDate;

    @Builder
    public SampleEntity(String categoryName, Date regDate) {
        this.categoryName = categoryName;
        this.regDate = regDate;
    }
}
