package com.joongna.send.data.parameter;

import lombok.Data;

/**
 * Package :: com.joongna.send.data.parameter
 * Developer :: Kang Tai-kyu(teckham7)
 * Date :: 2019-06-07
 * Description ::
 */
@Data
public class SampleParameter {
    private String application;
    private String label;
}
