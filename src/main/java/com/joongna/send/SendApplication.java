package com.joongna.send;

import com.joongna.send.properties.AwsS3Properties;
import com.joongna.send.properties.RedisProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@RefreshScope
@SpringBootApplication
@EnableAspectJAutoProxy
@EnableConfigurationProperties(value = {AwsS3Properties.class, RedisProperties.class})
public class SendApplication {

    public static void main(String[] args) {
        SpringApplication.run(SendApplication.class, args);
    }
}