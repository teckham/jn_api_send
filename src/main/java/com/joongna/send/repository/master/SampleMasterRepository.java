package com.joongna.send.repository.master;

import com.joongna.send.data.entity.SampleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Package :: com.joongna.send.repository.slave
 * Developer :: Kang Tai-kyu(teckham7)
 * Date :: 2019-06-07
 * Description ::
 */
public interface SampleMasterRepository extends JpaRepository<SampleEntity, Long> {

}
