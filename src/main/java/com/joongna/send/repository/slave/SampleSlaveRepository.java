package com.joongna.send.repository.slave;

import com.joongna.send.data.entity.SampleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Package :: com.joongna.send.repository.slave
 * Developer :: Kang Tai-kyu(teckham7)
 * Date :: 2019-06-07
 * Description ::
 */
public interface SampleSlaveRepository extends JpaRepository<SampleEntity, Long> {

    List<SampleEntity> findByCategorySeq(Long categorySeq);
}
