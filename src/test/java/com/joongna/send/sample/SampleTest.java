package com.joongna.send.sample;

import com.joongna.send.repository.slave.SampleSlaveRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Package :: com.joongna.send.sample
 * Developer :: Kang Tai-kyu(teckham7)
 * Date :: 2019-06-10
 * Description ::
 */
@SpringBootTest
public class SampleTest {

    @Autowired
    SampleSlaveRepository slaveRepository;

    @Test
    public void sampleTest()    {
        assert (slaveRepository.existsById(18L));
    }
}
