## 중고나라 Sending Project

Intro
---
    - Push, Mail, SMS 발송을 위한 Project로 활용하기 위해 Package 명을 'send'로 함.
    


Spec
---
    - Springboot :: 2.1.5.RELEASE
    - Gradle :: 5.4.1
    - JPA :: 2.1.5.RELEASE
    - Swagger2 & UI :: 2.9.2
    - Redis :: 2.1.5.RELEASE
    - Cloud Config :: 2.1.2.RELEASE
    - Cloud Bus AMQP :: 2.1.1.RELEASE
    - Maria DB Client :: 2.4.1
    - AWS SDK S3 :: 1.11.415 
    - Lombok :: 1.18.8
    - Actuator :: 2.1.5.RELEASE
    - Logback :: 1.2.3
    - Junit 5(Jupiter) :: 5.4.2 
    
Properties
---
    - Cloud Config 사용(bootstrap.yaml 참조)
    - Properties 객체화(com.joongna.send.properties)
    - Properties 암호화(Base64)
        - 계정 정보 등 노출 위험 정보는 암호화 필요.
        - Properties Class 암호화된 변수 Setter에서 Decode.
    
Utils.
---
    - Project 전반에 걸쳐 공통으로 사용될 기능 정의. 
    
    
Etc.
---
    - Yaml
    - banner.txt
        - http://patorjk.com/software/taag
    - HELP.md :: 기본 제공되는 Document Link
        - 새로운 Project 생성 시 .gitignore에 추가.
        
Gradle Execute
---
    - bootrun -Pprofile=local -Dspring.profiles.active=local